#pragma once

#include "SFML\Graphics.hpp"
using namespace sf;
class SpriteImage
{
public : 
	SpriteImage(Color _color, Vector2f _position, Texture _texture);
	void SetColour(Color _color);
	void SetPosition(Vector2f _position);
	void LoadTexture(Texture _texture);
	void SetTexture();
	Sprite GetSprite();

private: 
	Sprite _baseSprite;
	Vector2f _basePosition;
	Texture _baseTexture;
};