#pragma once

/* 
	Author : Kirsty Fraser 1203064
	Last edited : 16.4.16
	Version : 1.0

	Purpose : Give the basic definition for each of the Areas 
*/

//Application includes
#include "Card.h"

class Area
{
public : 
	Area();
	Area(EArea _name);
	Card DrawNewCard();
	inline EArea GetName() { return name; }
private : 
	EArea name;
};
