#pragma once

/*
	Author : Kirsty Fraser 1203064
	Last edited : 16.4.16
	Version : 1.0

	Purpose : Visual representation of the menu in which the user will interact with the game.
	Responsible for populating the menu with the appropriate visual elements.
*/

//SFML Library includes
#include <SFML\Graphics.hpp>
#include <SFML\System\Vector2.hpp>

//System includes 
#include <vector>
using namespace std;

//Application includes
#include "CharCard.h"
#include "ResourceCard.h"
#include "VisualManager.h"
#include "SpriteImage.h"

//Forward declarations
class VisualManager;

class ActionMenu
{
public : 
	ActionMenu(VisualManager* _vMan, EPlayer _playerNum);
	void RollDie();
	void MoveCharacter();
	void ShowCards();
	void ShowCharacters();
	void Attack();
	void Search();
	void Draw(sf::RenderWindow &window);
	void UpdateCharacter(int _charID, Character _character);
	void UpdateDie(vector<int> _actionDie);
	string ChooseDieSprite(int _dieCount);
	void CheckInputs(sf::Vector2i _mousePos);
	void AddCard(ECardType _type, int _value);
	void RemoveCard(int _cardID);
	ECharColour GetCharacterColor(int _charID);

private: 
	int GetHighlightedCharacter();
	int GetHighlightedDie();
	int GetHighlightedCard();
	
	VisualManager *vManager;						
	
	SpriteImage* dieOneSprite;						
	SpriteImage* dieTwoSprite;						
	SpriteImage* dieThreeSprite;
	SpriteImage* moveButton;
	SpriteImage* attackButton;
	SpriteImage* searchButton;
	SpriteImage* cardsButton;
	SpriteImage* characterButton;
	SpriteImage* rollButton;
	SpriteImage* turnButton;
	SpriteImage* placeCardButton;

	CharCard* char1;
	CharCard* char2;
	
	vector<ResourceCard*> cardDeck;
	ResourceCard* newCard; 

	int dieOne;
	int dieTwo;
	int dieThree;

	bool isShowingCards;
	bool isShowingCharacters;

	Vector2f menuStartingPoint; 

	const float PLAYERTWOOFFSET = 1100;
	const float STARTINGY = 700;
	const float DIEX = 700;
	const float DIEYSTART = 800;
	const float DIEYOFFSET = 80;
	const float BUTTONOFFSET = 100;
	const float CARDX = 200;
	const float CARDYSTART = 800;
	const float CARDOFFSET = 75;
	const Vector2f BUTTONSCALE = Vector2f(0.5f, 0.5f);

};
