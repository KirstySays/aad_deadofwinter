#ifndef _CHARCARD_H
#define _CHARCARD_H

#include "CardGraphic.h"
#include <string>
using namespace std;

#include "TextField.h"
#include "Enums.h"

class CharCard : public CardGraphic 
{
private:
	ECharColour charColor;
	TextField* nameText;
	TextField* attackText;
	TextField* searchText;
	SpriteImage* charSprite;

	const sf::Vector2f namePosition;
	const sf::Vector2f attackPosition;
	const sf::Vector2f searchPosition;
	const sf::Vector2f characterPosition;

	void AlignComponents();


public : 
	CharCard(ECharColour _id, Vector2f _startPosition);
	inline void CharCard::SetID(ECharColour _id) { charColor = _id; }
	inline void CharCard::SetNameText(string _name) { nameText->SetText(_name); }
	void CharCard::SetAttackText(int _attackNum); 
	void CharCard::SetSearchText(int _searchNum);
	void SetCharacterSpriteImage();
	inline bool IsHighlighted() { return isHighlighted; }
	//Inherited function
	void Draw(sf::RenderWindow& window);
	virtual void Align();
	inline ECharColour GetColor(){ return charColor; }
	
	
	
};
#endif