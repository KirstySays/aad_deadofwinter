#ifndef _STARTSCREEN_H
#define _STARTSCREEN_H

#include <SFML\System\Vector2.hpp>
using namespace sf;

#include "VisualManager.h"

class VisualManager;

class StartScreen
{
public : 
	StartScreen();
	void SetUp(VisualManager* _vMan);
	void Draw(sf::RenderWindow &_window);
	void CheckForInput(Vector2i mousePos);

private:
	Font font;
	Text startText;
	Sprite startButton;
	Texture startTexture;
	RenderWindow* window;
	VisualManager* vManager;
};
#endif