#include "Card.h"

/*
	Default constructor 
	@param N.A
	@return N.A
*/
Card::Card()
{

}

/*
	Overloaded constructor to allow for the setting of the card type
	@param _type : Defines the card type for this instance of the Card object.
	@return N.A
*/
Card::Card(ECardType _type)
{
	type = _type;
}

/*
	Initialisation of a new card object. Generation of a random number simulates the 
	chance of getting a card related to the Area that we are drawing from otherwise a 
	random card is provided back to the player.
	@param _area : tells us what area this card would be generated from.
	@return N.A
*/
void Card::Init(EArea _area)
{
	type = ECardType::UNDEFINED;

	srand(time(NULL));
	int random = rand() % 100;
	switch (_area)
	{
	case EArea::POLICE:
		if (random < 30)
		{
			type = ECardType::WEAPON;
		}
		break;
	case EArea::HOSPITAL:
		if (random < 30 )
		{
			type = ECardType::HEALTH;
		}
		break;
	case EArea::SCHOOL:
		if (random < 30)
		{
			type = ECardType::JUNK;
		}
		break;
	case EArea::GROCERY:
		if (random < 30)
		{
			type = ECardType::FOOD;
		}
		break;
	case EArea::GAS:
		if (random < 30)
		{
			type = ECardType::GAS;
		}
		break;
	case EArea::LIBRARY:
	{
		if (random < 30)
		{
			type = ECardType::BOOK;
		}
		break;
	}
	case EArea::STARTER :
		{
			int randomCard = rand() % 6 + 1;		//Number between 1 - 6
			type = (ECardType)randomCard;
		}
		
	}

	//Catches if no card has been chosen.
	if (type == ECardType::UNDEFINED)
	{
		int randomCard = rand() % 6 + 1;		//Number between 1 - 6
		type = (ECardType)randomCard;
	}

	int randomStat = rand() % 3 + 1;		//Number betwen 1-3
	statAmount = randomStat;
}