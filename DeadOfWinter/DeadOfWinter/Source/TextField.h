#pragma once
#include "SFML\Graphics\Font.hpp"
#include "SFML\Graphics\Text.hpp"
using namespace sf;

#include <string>
#include <iostream>
using namespace std;

class TextField
{

public:
	TextField();
	TextField(Color _color);
	TextField(Color _color, Vector2f _position);
	void SetColor(Color _color);
	void SetText(string _text);
	void LoadFont(string _path);
	void SetFont();
	Text GetText();
	void Move(Vector2f _move);

private : 
	Text baseText;
	Vector2f basePosition;
	Font baseFont;



};