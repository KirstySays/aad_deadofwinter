#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>
using namespace std;

#include "Enums.h"

class Character
{
private:
	string name;
	string colour;
	EArea position;
	int attackSkill;
	int searchSkill;
	int woundCount;

	inline void IncreaseWoundCount() { woundCount++; }

public : 
	inline int GetAttackSkill() { return attackSkill; }
	inline void SetAttackskill(int _attackSkill) { attackSkill = _attackSkill; }
	inline int GetSearchSkill() { return searchSkill; }
	inline void SetSearchSkill(int _searchSkill) { searchSkill = _searchSkill; }
	inline void SetPosition(EArea _newPosition) { position = _newPosition;	}
	inline EArea GetPosition() { return position; }
	inline void ResetWoundCount() { woundCount = 0; }
	inline void SetName(string _name) { name = _name; }
	inline string GetName(){ return name; }
	inline void SetColour(string _colour) { colour = _colour; }
	inline int GetWoundCount() { return woundCount; }

	bool RollForWound();
};
#endif