#include "Player.h"
#include <stdlib.h>
#include <time.h>

Player::Player()
{
	//default
}

Player::Player(CharacterPool* _cPool)
{
	characterPool = _cPool;
	for (int dieIterator = 0; dieIterator < 3; dieIterator++)
	{
		actionDie.push_back(0);
	}

	AddCharacter();
	AddCharacter();
}
Character Player::GetCharacter(int _id)
{
	return currentCharacter[_id];
}

Card Player::GetCard(int _id)
{
	return handDeck[_id];
}

void Player::AddNewCardToHand(Card _card)
{
	handDeck.push_back(_card);
}

void Player::RemoveCardFromHand(int _cardID)
{
	try
	{
		if (_cardID < handDeck.size())
			handDeck.erase(handDeck.begin() + _cardID);
		else
			throw(EErrorType::CARDID);
	}

	catch (EErrorType error_code)
	{
		if (error_code == EErrorType::CARDID)
		{
			cout << "[REMOVE CARD PLAYER] Unable to remove the card, position not found in the vector" << endl;
		}
	}


}

void Player::AddCharacter()
{
	if (currentCharacter.size() < 2)
	{
		Character newCharacter = characterPool->GenerateCharacter();
		currentCharacter.push_back(newCharacter);


	}
}

void Player::RemoveCharacter(int _charID)
{

	/*auto it = std::find(currentCharacter.begin(), currentCharacter.end(), _charID);
	if (it != currentCharacter.end())
		currentCharacter.erase(it);*/
}

EArea Player::GetCharacterPos(int _charID)
{
	return currentCharacter.at(_charID).GetPosition();
}

void Player::SetCharacterPos(int _charID, EArea _newPos)
{
	currentCharacter.at(_charID).SetPosition(_newPos);
}

void Player::RollDie()
{
	srand(time(NULL));


	for (int iterator = 0; iterator < 3; iterator++)
	{
		int randomNum = rand() % 6 + 1;
		actionDie.at(iterator) = randomNum;
		cout << "Die " << iterator << randomNum << endl;
	}
}

void Player::UseDie(int _dieID)
{
	actionDie.at(_dieID) = 0;
}

vector<int> Player::GetDie()
{
	return actionDie;

}
bool Player::ActionCheck(EAction _actionID, int _dieID, int _charID, int _cardID, EArea _newPos)
{
	EArea tempPosition = EArea::STARTER;
	if (_charID != 0)
	{
		tempPosition = currentCharacter.at(_charID-1).GetPosition();
		//throw (1);
	}

	
	switch (_actionID)
	{
	case EAction::ATTACK:
	{

		if (tempPosition == EArea::COLONY)
		{
			if (actionDie[_dieID-1] >= currentCharacter.at(_charID).GetAttackSkill())
			{
				UseDie(_dieID-1);
				return true;
			}
			else
			{
				std::cout << "Character doesn't have a high enough skill" << std::endl;
				return false;
			}

		}
		else
			std::cout << "Character is not at the colony" << std::endl;
		break;
	}
	case EAction::SEARCH:
	{

		if (tempPosition != EArea::COLONY)
		{
			if (actionDie[_dieID-1] >= currentCharacter.at(_charID-1).GetSearchSkill())
			{
				UseDie(_dieID-1);
				return true;
			}
			else
			{
				std::cout << "Character doesn't have a high enough skill" << std::endl;
				return false;
			}
		}
		else
			std::cout << "Character is at the colony" << std::endl;
		break;
	}

	case EAction::MOVE:
	{
		if (tempPosition != _newPos)
		{
			currentCharacter.at(_charID-1).SetPosition(_newPos);
			if (currentCharacter.at(_charID-1).RollForWound())
			{
				//TODO we need to update the visuals.				
			}
			cout << "character " << _charID << " has been moved" << endl;

			return true;			//This shows that the move has been successful.

		}
		else
		{
			std::cout << "Character is trying to move to the same position" << std::endl;
			return false;
		}

	}
	case EAction::ROLL: 
		RollDie();

	}

	
}
void Player::CheckForKilledCharacters()
{
	for (int iterator = 0; iterator < currentCharacter.size(); iterator++)
	{
		//TODO Write the Get Wound Count function in the character class
		/*if (currentCharacter.at(iterator).GetWoundCount() >= 3)
		{
			RemoveCharacter(iterator);
			AddCharacter();
		}*/
	}
}


	
