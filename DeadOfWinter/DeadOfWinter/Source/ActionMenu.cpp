#include "ActionMenu.h"

/*
	Constructor for the action menu object
	@param _vMan : instance of the visual manager
	@param _playerNum : the player number that we're building the object for
	@return N.A
*/
ActionMenu::ActionMenu(VisualManager* _vMan, EPlayer _playerNum)
{
	vManager = _vMan;
	
	if (_playerNum == EPlayer::ONE)
	{
		menuStartingPoint = Vector2f(0, STARTINGY);
		dieOneSprite = new SpriteImage(Color::White, Vector2f(DIEX, DIEYSTART));
		dieOneSprite->LoadTexture("Resources/Die/U1.png");

		dieTwoSprite = new SpriteImage(Color::White, Vector2f(DIEX, DIEYSTART+DIEYOFFSET));
		dieTwoSprite->LoadTexture("Resources/Die/U2.png");

		dieThreeSprite = new SpriteImage(Color::White, Vector2f(DIEX, DIEYSTART+DIEYOFFSET*2));
		dieThreeSprite->LoadTexture("Resources/Die/U3.png");

		moveButton = new SpriteImage(Color::White, Vector2f(0, STARTINGY));
		moveButton->LoadTexture("Resources/MoveButton.png");
		moveButton->SetScale(BUTTONSCALE);

		attackButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET, STARTINGY));
		attackButton->LoadTexture("Resources/AttackButton.png");
		attackButton->SetScale(BUTTONSCALE);

		searchButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET*2, STARTINGY));
		searchButton->LoadTexture("Resources/SearchButton.png");
		searchButton->SetScale(BUTTONSCALE);

		cardsButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET*3, STARTINGY));
		cardsButton->LoadTexture("Resources/CardsButton.png");
		cardsButton->SetScale(BUTTONSCALE);

		characterButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET*4, STARTINGY));
		characterButton->LoadTexture("Resources/CharacterButton.png");
		characterButton->SetScale(BUTTONSCALE);
		characterButton->Highlight();

		rollButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET*5, STARTINGY));
		rollButton->LoadTexture("Resources/RollButton.png");
		rollButton->SetScale(BUTTONSCALE);

		turnButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET*6, STARTINGY));
		turnButton->LoadTexture("Resources/TurnButton.png");
		turnButton->SetScale(BUTTONSCALE);

		placeCardButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET*7, STARTINGY));
		placeCardButton->LoadTexture("Resources/PlaceButton.png");
		placeCardButton->SetScale(BUTTONSCALE);

		char1 = new CharCard(ECharColour::BLUE, Vector2f(CARDX, CARDYSTART));
		char2 = new CharCard(ECharColour::GREEN, Vector2f(CARDX*2, CARDYSTART));

		
	}

	else if (_playerNum == EPlayer::TWO)
	{
		menuStartingPoint = Vector2f(PLAYERTWOOFFSET, 700);
		dieOneSprite = new SpriteImage(Color::White, Vector2f(DIEX + PLAYERTWOOFFSET, DIEYSTART));
		dieOneSprite->LoadTexture("Resources/Die/U1.png");

		dieTwoSprite = new SpriteImage(Color::White, Vector2f(DIEX + PLAYERTWOOFFSET, DIEYSTART+DIEYOFFSET));
		dieTwoSprite->LoadTexture("Resources/Die/U2.png");

		dieThreeSprite = new SpriteImage(Color::White, Vector2f(DIEX + PLAYERTWOOFFSET, DIEYSTART+DIEYOFFSET*2));
		dieThreeSprite->LoadTexture("Resources/Die/U3.png");

		moveButton = new SpriteImage(Color::White, Vector2f(0 + PLAYERTWOOFFSET, STARTINGY));
		moveButton->LoadTexture("Resources/MoveButton.png");
		moveButton->SetScale(BUTTONSCALE);

		attackButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET + PLAYERTWOOFFSET, STARTINGY));
		attackButton->LoadTexture("Resources/AttackButton.png");
		attackButton->SetScale(BUTTONSCALE);

		searchButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET * 2 + PLAYERTWOOFFSET, STARTINGY));
		searchButton->LoadTexture("Resources/SearchButton.png");
		searchButton->SetScale(BUTTONSCALE);

		cardsButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET * 3 + PLAYERTWOOFFSET, STARTINGY));
		cardsButton->LoadTexture("Resources/CardsButton.png");
		cardsButton->SetScale(BUTTONSCALE);

		characterButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET * 4 + PLAYERTWOOFFSET, STARTINGY));
		characterButton->LoadTexture("Resources/CharacterButton.png");
		characterButton->SetScale(BUTTONSCALE);
		characterButton->Highlight();

		rollButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET * 5 + PLAYERTWOOFFSET, STARTINGY));
		rollButton->LoadTexture("Resources/RollButton.png");
		rollButton->SetScale(BUTTONSCALE);

		turnButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET * 6 + PLAYERTWOOFFSET, STARTINGY));
		turnButton->LoadTexture("Resources/Button.png");
		turnButton->SetScale(BUTTONSCALE);

		placeCardButton = new SpriteImage(Color::White, Vector2f(BUTTONOFFSET * 7 + PLAYERTWOOFFSET, STARTINGY));
		placeCardButton->LoadTexture("Resources/PlaceButton.png");
		placeCardButton->SetScale(BUTTONSCALE);

		char1 = new CharCard(ECharColour::RED, Vector2f(CARDX + PLAYERTWOOFFSET, CARDYSTART));
		char2 = new CharCard(ECharColour::YELLOW, Vector2f(CARDX * 2 + PLAYERTWOOFFSET, CARDYSTART));
	}	
}

/*
	Sends a message back to the visual manager to inform the base code that the
	player wants to roll the die. 
	@param N.A
	@return N.A
*/
void ActionMenu::RollDie()
{
	vManager->SendToGameManager(NULL, NULL, EAction::ROLL, NULL, EArea::STARTER);
}

/*
	Obtains the highlighted character card before asking the visual manager to update the base 
	code if successfull
	@param N.A
	@return N.A
*/
void ActionMenu::MoveCharacter()
{ 
	try
	{
		int currentCharacter = GetHighlightedCharacter();
		if (currentCharacter == 0)
			throw (EErrorType::CHARACTERHIGHLIGHT);

		vManager->SendToGameManager(NULL, currentCharacter, EAction::MOVE, NULL, EArea::STARTER);

	}
	
	catch (EErrorType errorCode)
	{
		if (errorCode == EErrorType::CHARACTERHIGHLIGHT)
			cout << "[MOVE]Character card not selected" << endl;
	}		
	
}

/*
	Displays the cards which the player has in their current hand deck. 
	@param N.A
	@return N.A
*/
void ActionMenu::ShowCards()
{
	isShowingCards = true;
	isShowingCharacters = false;
	Vector2f offset = menuStartingPoint;
	offset.y = CARDYSTART;

	for each (ResourceCard* rCard in cardDeck)
	{
		rCard->Move(Vector2f(offset));
		offset.x += CARDOFFSET;
	}
}

/*
	Obtains the highlighted character and die then asks the visual manager 
	to update the base code based on the attack action,  if successfull
	@param N.A
	@return N.A
*/
void ActionMenu::Attack()
{
	try
	{
		int currentCharacter = GetHighlightedCharacter();
		
		if (currentCharacter == 0)
			throw (EErrorType::CHARACTERHIGHLIGHT);

		int dieID = GetHighlightedDie();
		
		if (dieID == 0)
			throw (EErrorType::DIEHIGHLIGHT);

		vManager->SendToGameManager(dieID, currentCharacter, EAction::ATTACK, NULL, EArea::STARTER);
	}

	catch (int error_code)
	{
		if (error_code == 0)
			cout << "[ATTACK]No character selected" << endl;
		else if (error_code == 1)
			cout << "[ATTACK]No die selected" << endl;
	}

}

/*
	Obtains the highlighted character and die then asks the visual manager
	to update the base code based on the search section, if succcesfull
	@param N.A
	@return N.A
*/
void ActionMenu::Search()
{
	try
	{
		int currentCharacter = GetHighlightedCharacter();

		if (currentCharacter == 0)
			throw (EErrorType::CHARACTERHIGHLIGHT);

		
		int dieID = GetHighlightedDie();

		if (dieID == 0)
			throw (EErrorType::DIEHIGHLIGHT);
		
		
		vManager->SendToGameManager(dieID, currentCharacter, EAction::SEARCH, NULL, EArea::STARTER);
	}

	catch (EErrorType error_code)
	{
		if (error_code == EErrorType::CHARACTERHIGHLIGHT)
		{
			cout << "[SEARCH] No character selected" << endl;
		}
		else if (error_code == EErrorType::DIEHIGHLIGHT)
		{
			cout << "[SEARCH] No die Selected " << endl;
		}
	}
}

/*
	Draws all elements onto the screen, depending on the current menu selection
	@params window : instance of the Render Window that we're using as the screen for the player
	@return N.A
*/
void ActionMenu::Draw(sf::RenderWindow &window)
{

	window.draw(dieOneSprite->GetSprite());
	window.draw(dieTwoSprite->GetSprite());
	window.draw(dieThreeSprite->GetSprite());
	window.draw(moveButton->GetSprite());
	window.draw(attackButton->GetSprite());
	window.draw(searchButton->GetSprite());
	window.draw(cardsButton->GetSprite());
	window.draw(characterButton->GetSprite());
	window.draw(rollButton->GetSprite());
	window.draw(turnButton->GetSprite());
	window.draw(placeCardButton->GetSprite());

	if (isShowingCards)
	{
		for each (ResourceCard* rCard in cardDeck)
		{
			rCard->Draw(window);	
		}	
	}
	else 
	{
		char1->Draw(window);
		char2->Draw(window);
	}	
}

/*
	Updates the character card with the information related to the characters
	attack and search skill. 
	@param _charID : ID for the character so we can update the correct one
	@param _character : Character object which will provide the information needed. 
	@return N.A
*/
void ActionMenu::UpdateCharacter(int _charID, Character _character)
{
	if (_charID == 1)
	{
		char1->SetAttackText(_character.GetAttackSkill());
		char1->SetSearchText(_character.GetSearchSkill());
	}
	else if (_charID == 2)
	{
		char2->SetAttackText(_character.GetAttackSkill());
		char2->SetSearchText(_character.GetSearchSkill());
	}
}

/* 
	Updates the die visuals to represent the current numbers that the dies are 
	after being freshly rolled
	@param _actionDie : the set of three dies values after being rolled
	@return N.A
*/
void ActionMenu::UpdateDie(vector<int> _actionDie)
{
	dieOne = _actionDie[0];
	dieTwo = _actionDie[1];
	dieThree = _actionDie[2];

	//Update sprites
	string tempTexturePathOne = ChooseDieSprite(dieOne);
	dieOneSprite->LoadTexture(tempTexturePathOne);

	string tempTexturePathTwo = ChooseDieSprite(dieTwo);
	dieTwoSprite->LoadTexture(tempTexturePathTwo);

	string tempTexturePathThree = ChooseDieSprite(dieThree);
	dieThreeSprite->LoadTexture(tempTexturePathThree);

}

/*
	Ensures that the sprite represented the number that the die should represent
	@param _dieCount : the number that we need the die to represent
	@return path : the filepath for the sprite image that is required 
*/
string ActionMenu::ChooseDieSprite(int _dieCount)
{
	string path;
	switch (_dieCount)
	{
	case 1:
		path = "Resources/Die/1.png";
		break;
	case 2:
		path = "Resources/Die/2.png";
		break;
	case 3:
		path = "Resources/Die/3.png";
		break;
	case 4:
		path = "Resources/Die/4.png";
		break;
	case 5:
		path = "Resources/Die/5.png";
		break;
	case 6:
		path = "Resources/Die/6.png";
		break;

	}
	return path;
}

/*
	Checks each of the elements to determine if the mouse click was within 
	their image. Deals with the highlighting / dehighting of the appropriate images
	before requesting the action to be carried out 
	@param _mousePos : the position of the mouseclick onthe screen 
	@return N.A
*/
void ActionMenu::CheckInputs(Vector2i _mousePos)
{
	if (rollButton->CheckBounds(_mousePos))
	{
		moveButton->DeHighlight();
		attackButton->DeHighlight();
		searchButton->DeHighlight();
		cardsButton->DeHighlight();
		characterButton->DeHighlight();
		turnButton->DeHighlight();
		placeCardButton->DeHighlight();
		RollDie();
	}
		
	else if (moveButton->CheckBounds(_mousePos))
	{
		rollButton->DeHighlight();
		attackButton->DeHighlight();
		searchButton->DeHighlight();
		cardsButton->DeHighlight();
		characterButton->DeHighlight();
		turnButton->DeHighlight();
		placeCardButton->DeHighlight();
		MoveCharacter();
	}
		
	else if (attackButton->CheckBounds(_mousePos))
	{
		moveButton->DeHighlight();
		rollButton->DeHighlight();
		searchButton->DeHighlight();
		cardsButton->DeHighlight();
		characterButton->DeHighlight();
		turnButton->DeHighlight();
		placeCardButton->DeHighlight();
		Attack();
	}
		
	else if (searchButton->CheckBounds(_mousePos))
	{
		moveButton->DeHighlight();
		attackButton->DeHighlight();
		rollButton->DeHighlight();
		cardsButton->DeHighlight();
		characterButton->DeHighlight();
		turnButton->DeHighlight();
		placeCardButton->DeHighlight();
		Search();
	}
		
	else if (cardsButton->CheckBounds(_mousePos))
	{
		moveButton->DeHighlight();
		attackButton->DeHighlight();
		searchButton->DeHighlight();
		rollButton->DeHighlight();
		characterButton->DeHighlight();
		turnButton->DeHighlight();
		placeCardButton->DeHighlight();
		ShowCards();
	}
		
	else if (characterButton->CheckBounds(_mousePos))
	{
		moveButton->DeHighlight();
		attackButton->DeHighlight();
		searchButton->DeHighlight();
		cardsButton->DeHighlight();
		rollButton->DeHighlight();
		turnButton->DeHighlight();
		placeCardButton->DeHighlight();
		ShowCharacters();
	}
	else if (turnButton->CheckBounds(_mousePos))
	{
		moveButton->DeHighlight();
		attackButton->DeHighlight();
		searchButton->DeHighlight();
		cardsButton->DeHighlight();
		characterButton->DeHighlight();
		rollButton->DeHighlight();
		placeCardButton->DeHighlight();
		vManager->SendToGameManager(0, 0, EAction::TURN, 0, EArea::NONE);
	}

	else if (placeCardButton->CheckBounds(_mousePos))
	{
		moveButton->DeHighlight();
		attackButton->DeHighlight();
		searchButton->DeHighlight();
		cardsButton->DeHighlight();
		characterButton->DeHighlight();
		rollButton->DeHighlight();
		turnButton->DeHighlight();

		vManager->SendToGameManager(0, 0, EAction::CARD, GetHighlightedCard(), EArea::NONE);
	
		
	}
	
	if (dieOneSprite->CheckBounds(_mousePos))
	{
		dieTwoSprite->DeHighlight();
		dieThreeSprite->DeHighlight();
	}
	else if (dieTwoSprite->CheckBounds(_mousePos))
	{
		dieOneSprite->DeHighlight();
		dieThreeSprite->DeHighlight();
	}
	else if (dieThreeSprite->CheckBounds(_mousePos))
	{
		dieOneSprite->DeHighlight();
		dieTwoSprite->DeHighlight();
	}

	if (char1->CheckBounds(_mousePos))
		char2->DeHighlight();
	else if (char2->CheckBounds(_mousePos))
		char1->DeHighlight();

	for each (ResourceCard* rCard in cardDeck)
	{
		rCard->CheckBounds(_mousePos);
	}
		
}

/*
	Adds a new visual card into the card deck for the player to view its attributes
	@param _type : The type of resources that the card carries
	@param _value : The stat value for the card
	@return N.A
*/
void ActionMenu::AddCard(ECardType _type, int _value)
{
	newCard = new ResourceCard(_type, _value);	
	cardDeck.push_back(newCard);
}

/* 
	Removes the card from the visual deck for the player, if the card is present
	@param _cardID : the ID for the card that has been requested to be removed
	@return N.A
*/
void ActionMenu::RemoveCard(int _cardID)
{
	try
	{

		if (_cardID < cardDeck.size())
			cardDeck.erase(cardDeck.begin() + _cardID);
		else
			throw(EErrorType::CARDID);
	}
	
	catch (EErrorType error_code)
	{
		if (error_code == EErrorType::CARDID)
		{
			cout << "[REMOVE CARD ACTION MENU] Unable to remove the card, position not found in the vector" << endl;
		}
	}
	
}

/*
	Displays the character cards to the screen
	@param N.A
	@return N.A
*/
void ActionMenu::ShowCharacters()
{
	isShowingCards = false;
	isShowingCharacters = true;
}

/*
	Obtains the color of the character
	@param _charID : the ID for the function to know what character to ask for the color
	@return ECharColour : the colour for the character requested.
*/
ECharColour ActionMenu::GetCharacterColor(int _charID)
{
	if (_charID == 1)
		return char1->GetColor();
	else if (_charID == 2)
		return char2->GetColor();
}

/*
	Obtains the current highlighted character 
	@param N.A
	@return int : numberID for the character that is chosen, or zero if no characters are highlighted.

*/
int ActionMenu::GetHighlightedCharacter()
{
	if (char1->IsHighlighted())
	{
		return 1;
	}
	else if (char2->IsHighlighted())
	{
		return 2;
	}
	else
		return 0;	
}

/*
	Obtains the current highlighted die 
	@param N.A
	@return int : NumberID for the die which is currently highlighted, or zero if no die are highlighted
*/
int ActionMenu::GetHighlightedDie()
{
	if (dieOneSprite->IsHighlighted())
		return 1;
	else if (dieTwoSprite->IsHighlighted())
		return 2;
	else if (dieThreeSprite->IsHighlighted())
		return 3;
	else
		return 0;
}

/* 
	Obtains the highlighted card from the players card deck
	@params N.A
	@returns int : the ID for card that is highlighted, zero if there are no cards selected.
*/
int ActionMenu::GetHighlightedCard()
{
	try
	{
		if (cardDeck.size() != 0)
		{
			for (int cardIterator = 0; cardIterator < cardDeck.size(); cardIterator++)
			{
				if (cardDeck.at(cardIterator)->IsHighlighted())
				{
					return cardIterator;
				}
			}
			return 0;			//Error will be collected further up the callstack
		}
		else
			throw(EErrorType::NOCARDS);
		
	}
	catch (EErrorType error_code)
	{
		if (error_code == EErrorType::NOCARDS)
			cout << "[GET HIGHLIGHTED CARD] No cards currently in the deck." << endl;
	
	}
	
}
