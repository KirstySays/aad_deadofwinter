#include "StartScreen.h"

StartScreen::StartScreen()
{
	
	if (!font.loadFromFile("Resources/Vec.ttf"))
		cout << "Font arial.ttf was not loaded into sf::Font font [Start Screen]" << endl;
	else
		startText.setFont(font);

	startText.setCharacterSize(24);
	startText.setPosition(sf::Vector2f(100, 50));
	startText.setColor(sf::Color::White);
	startText.setString("Welcome");

	if (!startTexture.loadFromFile("Resources/start.png"))
		cout << "Texture start.png was not laoded into sf::Texture texture [Start Screen]" << endl;
	else
		startButton.setTexture(startTexture);

	startButton.setPosition(sf::Vector2f(100, 100));
	startButton.setScale(sf::Vector2f(1, 1));

	cout << "Startscreen constructor called" << endl;
}

void StartScreen::SetUp(VisualManager* _vMan)
{
	vManager = _vMan;
}

void StartScreen::Draw(sf::RenderWindow &_window)
{
	
	_window.draw(startText);
	_window.draw(startButton);
	
}

void StartScreen::CheckForInput(Vector2i mousePos)
{
	FloatRect bounds = startButton.getGlobalBounds();

	float mouseX = mousePos.x;
	float mouseY = mousePos.y;


	if (bounds.contains(mouseX, mouseY))
	{
		//Game manager needs to be accepting of starting the game
		cout << "Start button pressed" << endl;
		vManager->SendToGameManager(NULL, NULL, EAction::START, NULL, EArea::STARTER);
	}
}