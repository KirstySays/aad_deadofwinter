#pragma once

/*
	Author : Kirsty Fraser 1203064
	Last edited : 16.4.16
	Version : 1.0

	Purpose : Visual representation of the Area instances. Allows for the updating of the variables.
*/

//System includes
#include <vector>
using namespace std;

//Application includes
#include "VisualManager.h"
#include "SpriteImage.h"
#include "TextField.h"

//Forward declarationms
class VisualManager;

class AreaGraphic
{
public : 
	AreaGraphic();
	AreaGraphic(EArea _areaType, VisualManager* _vMan, Vector2f _startPosition);
	void RemoveCharacter(ECharColour _charID);
	void AddCharacter(ECharColour _charID);
	void Draw(sf::RenderWindow& window);
	void Highlight();
	void Dehighlight();
	bool IsHighlighted();
	void CheckInput(sf::Vector2i _mousePos);
	inline EArea GetAreaType() { return areaType; }
	
protected: 
	void AlignComponents();

	SpriteImage* cardBG;
	SpriteImage* areaImage;
	SpriteImage* slotOneImage;
	SpriteImage* slotTwoImage;
	int charactersPresent;
	VisualManager* vManager;
	TextField* areaText;
	EArea areaType;
	bool isHighlighted;

	const int MAXCHARALLOCATION = 2;
};
