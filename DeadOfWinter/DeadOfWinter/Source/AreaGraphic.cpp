#include "AreaGraphic.h"

/*
	Default constructor
	@param N.A
	@return N.A
*/
AreaGraphic::AreaGraphic()
{

}

/*
	Overloaded constructor to handle the initialisation of areaType, the instance of the VisualManager
	and a starting position on the screen for the image. An appropriate sprite is chosen for the area 
	based on it's area type.
	@param _areaType : defines what area we are initialising.
	@param _vMan : provides us with the instance of the Visual Manager so the class can call related
	functions at a later date
	@param _startPosition : starting x and y position for the image to be drawn from on screen.
	@return N.A
*/

AreaGraphic::AreaGraphic(EArea _areaType, VisualManager* _vMan, Vector2f _startPosition)
{
	string filepath;
	string areaName;
	
	switch (_areaType)
	{
	case EArea::SCHOOL : 
		filepath = "Resources/school.png";
		areaName = "School";
		break;
	case EArea::POLICE : 
		filepath = "Resources/police.png";
		areaName = "Police Station";
		break;
	case EArea::GAS:
		filepath = "Resources/gasStation.png";
		areaName = "Gas Station";
		break;
	case EArea::HOSPITAL:
		filepath = "Resources/hospital.png";
		areaName = "Hospital";
		break;
	case EArea::LIBRARY: 
		filepath = "Resources/library.png";
		areaName = "Library";
		break;
	case EArea::GROCERY:
		filepath = "Resources/grocery.png";
		areaName = "Grocery Store";
		break;


	}
	cardBG = new SpriteImage(Color::White, _startPosition);
	cardBG->LoadTexture("Resources/glassPanel.png");
	cardBG->SetScale(Vector2f(3, 3));

	areaImage = new SpriteImage(Color::White, Vector2f(_startPosition.x + 200, _startPosition.y));
	areaImage->LoadTexture(filepath);

	slotOneImage = new SpriteImage(Color::White, Vector2f(_startPosition.x + 25, _startPosition.y + 50));
	slotOneImage->LoadTexture("Resources/White.png");

	slotTwoImage = new SpriteImage(Color::White, Vector2f(_startPosition.x + 25, _startPosition.y + 125));
	slotTwoImage->LoadTexture("Resources/White.png");

	areaText = new TextField(Color::Black, Vector2f(_startPosition.x + 50, _startPosition.y-3));
	areaText->LoadFont("Resources/Vec.ttf");
	areaText->SetText(areaName);
	vManager = _vMan;

	areaType = _areaType;
	charactersPresent = 0;
}

/* 
	Deletes a character from the location, normally after they have moved. Uses the colour to 
	check if they are present and if so, turns the sprite back to white again.
	@param _charID  : identification for the characters colour
	@return N.A
*/
void AreaGraphic::RemoveCharacter(ECharColour _charID)
{
	Color colorToCheck;
	colorToCheck == Color::White;
	//Check if this area has that color being used. 
	switch (_charID)
	{
	case ECharColour::BLUE:
		colorToCheck = Color::Blue;
		break;
	case ECharColour::GREEN:
		colorToCheck = Color::Green;
		break;
	case ECharColour::RED:
		colorToCheck = Color::Red;
		break;
	case ECharColour::YELLOW:
		colorToCheck = Color::Yellow;
		break;
		
	}

	if (slotOneImage->GetSprite().getColor() == colorToCheck)
	{

		slotOneImage->SetColour(Color::White);
		charactersPresent--;
	}
	else if (slotTwoImage->GetSprite().getColor() == colorToCheck)
	{
		slotTwoImage->SetColour(Color::White);
		charactersPresent--;
	}
		
}

/*
	Adds a character to the location, normally after they have moved. Uses the colour identification
	passed in to change the colour of the sprite.
	@param _charID  : identification for the characters colour
	@return N.A
*/
void AreaGraphic::AddCharacter(ECharColour _charID)
{
	Color newColor;
	switch (_charID)
	{
	case ECharColour::BLUE:
		newColor = Color::Blue;
		break;
	case ECharColour::GREEN:
		newColor = Color::Green;
		break;
	case ECharColour::RED:
		newColor = Color::Red;
		break;
	case ECharColour::YELLOW:
		newColor = Color::Yellow;
		break;
	}

	if (charactersPresent < MAXCHARALLOCATION)
	{
		if (charactersPresent == 0)
		{
			slotOneImage->SetColour(newColor);
			charactersPresent++;
		}
		else
		{
			slotTwoImage->SetColour(newColor);
			charactersPresent++;
		}	
	}
}

/*
	Draws the visual elements to the screen for the players to see 
	@param window : Instance of the render window that can be used to reference the window
	that we're using to draw onto the screen.
	@return N.A
*/
void AreaGraphic::Draw(sf::RenderWindow& window)
{
	
	window.draw(cardBG->GetSprite());
	window.draw(areaImage->GetSprite());
	window.draw(slotOneImage->GetSprite());
	window.draw(slotTwoImage->GetSprite());
	window.draw(areaText->GetText());
}

/*
	Changes the sprites background to give the appearance of the area being highlighted / selected.
	Updates the state of the sprite as well.
	@param N.A
	@return N.A
*/
void AreaGraphic::Highlight()
{
	isHighlighted = true;
	cardBG->SetColour(Color::Cyan);
	
}

/*
	Changes the sprites background to give the appearance of the area being back to normal / deselected
	Updates the state of the sprite as well.
	@param N.A
	@return N.A
*/
void AreaGraphic::Dehighlight()
{
	isHighlighted = false;
	cardBG->SetColour(Color::White);
}


/*
	Used for querying to see if the Area is highlighted or not. 
	@param N.A
	@return isHighlighted : bool used to say if the AreaGraphic is highlighted or not.
*/
bool AreaGraphic::IsHighlighted()
{
	return isHighlighted;
}


/*
	Queries the AreaGraphics instance background against the mouse's position on clicking 
	to see if it has been clicked on. If so, the image is asked to be Highlighted.
	@param _mousePos : the x and y coordinates of the mouse click on the screen,
	@return N.A
*/
void AreaGraphic::CheckInput(sf::Vector2i _mousePos)
{
	if (cardBG->CheckBounds(_mousePos))
	{
		Highlight();
	}
	else
		Dehighlight();
}