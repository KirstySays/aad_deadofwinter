#include "TextField.h"

TextField::TextField()
{
	baseText.setCharacterSize(16);
	
	
}

TextField::TextField(Color _color)
{
	baseText.setColor(_color);
	LoadFont("Resources/Vec.ttf");
	baseText.setCharacterSize(16);
}
TextField::TextField(Color _color, Vector2f _position)
{
	baseText.setColor(_color);
	baseText.setPosition(_position);
	baseText.setCharacterSize(16);
	LoadFont("Resources/Vec.ttf");
}

Text TextField::GetText()
{
	return baseText;
}

void TextField::Move(Vector2f _move)
{
	baseText.setPosition(_move);
}

void TextField::LoadFont(string _path)
{
	if (!baseFont.loadFromFile(_path))
		std::cout << "Unable to load font from the filepath" << std::endl;
	else
		SetFont();
}

void TextField::SetColor(Color _color)
{
	baseText.setColor(_color);
}

void TextField::SetFont()
{
	baseText.setFont(baseFont);
}

void TextField::SetText(string _text)
{
	baseText.setString(_text);
}