#include "SpriteImage.h"

SpriteImage::SpriteImage()
{

}

SpriteImage::SpriteImage(Color _color)
{
	baseSprite.setColor(_color);
}
SpriteImage::SpriteImage(Color _color, Vector2f _basePosition)
{
	baseSprite.setColor(_color);
	baseSprite.setPosition(_basePosition);
	isHighlighted = false;
}

void SpriteImage::Move(Vector2f move)
{
	/*Vector2f currentPosition = baseSprite.getPosition();
	currentPosition += move;*/
	SetPosition(move);
}
void SpriteImage::Highlight()
{
	isHighlighted = true;
	SetColour(Color::Cyan);
}

void SpriteImage::DeHighlight()
{
	isHighlighted = false;
	SetColour(Color::White);
}
void SpriteImage::SetScale(Vector2f _scale)
{
	baseSprite.setScale(_scale);
}

void SpriteImage::SetColour(Color _color)
{
	baseSprite.setColor(_color);
}

void SpriteImage::SetPosition(Vector2f _position)
{
	basePosition = _position;
	baseSprite.setPosition(basePosition);
}

Sprite SpriteImage::GetSprite()
{
	return baseSprite;
}

void SpriteImage::LoadTexture(string _path)
{
	if (!baseTexture.loadFromFile(_path))
	{
		cout << "[SPRITE IMAGE] UNABLE TO LOAD TEXTURE FROM PATH " << _path << endl;
	}
	else
	{
		SetTexture();
	}
}

void SpriteImage::SetTexture()
{
	baseSprite.setTexture(baseTexture);
}

bool SpriteImage::CheckBounds(Vector2i _position)
{
	FloatRect bounds = baseSprite.getGlobalBounds();

	if (bounds.contains(_position.x, _position.y))
	{
		Highlight();
		return true;
	}
	else
		return false;
}
