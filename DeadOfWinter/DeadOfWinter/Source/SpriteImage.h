#pragma once

#include "SFML\Graphics.hpp"
using namespace sf;

#include <string>
#include <iostream>
using namespace std;

class SpriteImage
{
public : 
	SpriteImage();
	SpriteImage(Color _color);
	SpriteImage(Color _color, Vector2f _position);
	void Move(Vector2f _move);
	void SetColour(Color _color);
	void SetPosition(Vector2f _position);
	void SetScale(Vector2f _scale);
	void LoadTexture(string _path);
	void SetTexture();
	Sprite GetSprite();
	void Highlight();
	void DeHighlight();
	inline bool IsHighlighted(){ return isHighlighted; }
	bool CheckBounds(Vector2i _position);

private: 
	Sprite baseSprite;
	Vector2f basePosition;
	Texture baseTexture;
	bool isHighlighted;
};