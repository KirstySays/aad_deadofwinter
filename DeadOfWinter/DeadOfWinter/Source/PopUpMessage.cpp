#include "PopUpMessage.h"

PopUpMessage::PopUpMessage()
{
}

PopUpMessage::PopUpMessage(Vector2f _startPos, VisualManager* _vManager)
{
	
	backgroundImage = new SpriteImage(Color::White, _startPos);
	backgroundImage->LoadTexture("Resources/yellow_panel.png");
	backgroundImage->SetScale(Vector2f(8, 2));

	messageText = new TextField(Color::Black, _startPos + MESSAGETEXTOFFSET);
	messageText->SetText("TESTING");

	buttonSprite = new SpriteImage(Color::White, _startPos + BUTTONOFFSET);
	buttonSprite->LoadTexture("Resources/Button.png");

	vManager = _vManager;
	
}

void PopUpMessage::UpdateMessageText(string _message)
{
	messageText->SetText(_message);
}


void PopUpMessage::Draw(sf::RenderWindow& window)
{
	window.draw(backgroundImage->GetSprite());
	window.draw(messageText->GetText());
	window.draw(buttonSprite->GetSprite());

	
}

void PopUpMessage::Clear()
{
	messageText->SetText("");
}

void PopUpMessage::CheckForInput(Vector2i mousePos)
{
	if (buttonSprite->CheckBounds(mousePos))
	{
		vManager->SendToGameManager(NULL, NULL, EAction::UNPAUSE, NULL, EArea::NONE);
	}
}
