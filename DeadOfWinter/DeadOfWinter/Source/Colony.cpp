#include "Colony.h";
#include <math.h>>

Colony::Colony()
{
	helplessSurvivors = 2;
	zombieCount = 4;
	barrierCount = 0;
}
bool Colony::CheckFood()
{
	int totalFood = foodResources * 2;
	int remainingFood = totalFood - helplessSurvivors;

	if (remainingFood > 0)
	{
		foodResources = remainingFood * 0.5f;
		return true;
	}
	else
	{
		foodResources = 0;
		return false;
	}
	
}

bool Colony::CheckCrisis(ECardType _type, int _minAmount)
{
	int cardCount = 0;
	for each (Card tempCard in crisisResources)
	{
		if (tempCard.GetType() == _type)
		{
			cardCount+=tempCard.GetStatAmount();
		}
	}

	if (cardCount >= _minAmount)
	{
		return true;
	}
	else
		return false;

}

void Colony::RecieveCard(Card &card)
{
	//TODO Check the cards type
	//If its food, add one to the food resources
	if (card.GetType() == ECardType::FOOD)
		AddToFoodResources(card.GetStatAmount());
	else 
		AddToCrisisResources(card);
		
}

bool Colony::CheckZombieCount()
{
	if (zombieCount >= MAXZOMBIE)
	{ 
		return false;
	}
	return true;
}