#ifndef _COLONY_H
#define _COLONY_H

#include "Area.h"
#include "Enums.h"
#include <vector>
using namespace std;

class Colony:Area
{
private: 
	int foodResources;
	vector<Card> crisisResources;
	int helplessSurvivors;
	int zombieCount;
	int barrierCount;
	const int MAXCOLONYSPACE = 10;
	const int MAXZOMBIE = 15;

public:
	inline void AddToFoodResources(int _value) { foodResources+=_value; }
	inline int GetFoodResources() { return foodResources; }

	inline void AddToCrisisResources(Card _contributionCard) { crisisResources.push_back(_contributionCard); }
	inline void ResetCrisisResources() { crisisResources.clear(); }
	inline int GetCrisisResources(){ return crisisResources.size(); }
	inline int GetHelplessSurivors() { return helplessSurvivors; }
	inline void AmendHelplessSurvivors(int _amount) { helplessSurvivors += _amount; }
	inline void AddZombie(int count) { zombieCount += count; }
	inline void RemoveZombie() { zombieCount--; }
	inline int GetZombieCount() { return zombieCount; }
	inline void AddBarrier() { barrierCount++; }
	inline void RemoveBarrier() { barrierCount--; }
	bool CheckZombieCount();
	Colony();
	void RecieveCard(Card &card);
	bool CheckFood();
	bool CheckCrisis(ECardType _type, int _minAmount);


};
#endif

