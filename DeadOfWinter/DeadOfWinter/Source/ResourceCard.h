#ifndef _RESOURCECARD_H
#define _RESOURCECARD_H

#include "CardGraphic.h"
#include "SpriteImage.h"
#include "TextField.h"

#include "Enums.h"
#include <iostream>
#include <string>
using namespace std;

class ResourceCard : public CardGraphic
{
public : 
	ResourceCard();
	ResourceCard(ECardType _type, int _value);
	void Move(Vector2f _move);
	void AlignComponents();
	void Draw(sf::RenderWindow& window);
	
private: 
	SpriteImage* typeImage;
	TextField* cardValueText;
	TextField* cardTypeText;
};
#endif