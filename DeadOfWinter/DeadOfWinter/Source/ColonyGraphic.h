#ifndef COLONYGRAPHIC_H
#define COLONYGRAPHIC_H

#include <SFML/System/Vector2.hpp>
#include <SFML\Graphics\Text.hpp>
#include <SFML\Graphics.hpp>
using namespace sf;

#include <vector>
using namespace std;

//forward declarations
#include "AreaGraphic.h"			//need to have the base class in here.
#include "SpriteImage.h"
#include "TextField.h"

class ColonyGraphic  
{

private:
	TextField* foodText;
	TextField* crisisContribution;
	TextField* survivorCount;
	TextField* zombieCount;
	TextField* barrierCount;
	TextField* roundText;
	TextField* moraleText;
	TextField* areaText;

	int charactersPresent;
	const int MAXCHARALLOCATION = 4;
	SpriteImage* slotOneImage;
	SpriteImage* slotTwoImage;
	SpriteImage* slotThreeImage;
	SpriteImage* slotFourImage;
	SpriteImage* backgroundImage;
	SpriteImage* areaImage;

	bool isHighlighted;
	EArea area;
	
public : 
	ColonyGraphic(EArea _area);
	void UpdateZombies(int _count); 
	inline EArea GetArea() { return area; }
	void AddCharacter(ECharColour _charID);
	void RemoveCharacter(ECharColour _charID);
	void UpdateSurvivors(int _count);
	void UpdateFoodText(int _count); 
	void UpdateContribution(int _count); 
	void UpdateRoundNumber(int _round); 
	void UpdateMoraleNumbers(int _morale); 
	void Draw(sf::RenderWindow& window);
	void AlignComponents();
	void CheckInput(sf::Vector2i mousePos);
	inline bool IsHighlighted() { return isHighlighted; }
	void Highlight();
	void DeHighlight();

};
#endif