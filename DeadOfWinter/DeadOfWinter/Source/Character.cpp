#include "Character.h"
#include <time.h>
#include <stdlib.h>

bool Character::RollForWound()
{
	srand(time(NULL));
	int randomNum = rand() % 2;

	if (randomNum == 1)
	{
		IncreaseWoundCount();
		return true;
	}
	else
		return false;

}

