#include "CardGraphic.h"

CardGraphic::CardGraphic()
{
	//deafult
}

void CardGraphic::Draw(sf::RenderWindow &window)
{
	//TODO once we have a reference to the window
	

	window.draw(cardBG->GetSprite());


	
}

void CardGraphic::Align()
{
	
}

void CardGraphic::Update()
{
	//Might not be needed but is a base function
}

void CardGraphic::Highlight()
{
	isHighlighted = true;
	cardBG->SetColour(Color::Cyan);

}

void CardGraphic::DeHighlight()
{
	isHighlighted = false;
	cardBG->SetColour(Color::White);
}

bool CardGraphic::IsHighlighted()
{
	return isHighlighted;
}
bool CardGraphic::CheckBounds(Vector2i mousePos)
{
	if (cardBG->CheckBounds(mousePos))
	{
		Highlight();
		return true;
	}

	else

		return false;
	
}