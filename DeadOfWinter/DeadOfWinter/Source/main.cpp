#include <iostream>
#include <SFML/Graphics.hpp>

#include "GameManager.h"
#include "VisualManager.h"

int main()
{	
	//Set up initial window
	sf::RenderWindow window(sf::VideoMode::getDesktopMode(), "Dead Of Winter");
	window.setVerticalSyncEnabled(true);	
	window.setKeyRepeatEnabled(false);

	//Set up managers
	GameManager* gManager = new GameManager();
	VisualManager* vManager = new VisualManager();

	gManager->SetUpGame(vManager);
	vManager->Setup(gManager, window);

	
	//main loop
	while (window.isOpen())
	{		
		sf::Event event;	
		while (window.pollEvent(event))	
		{	
			if (event.type == sf::Event::Closed)	
				window.close();	

			if (event.type == event.MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
			{
				sf::Vector2i coords = sf::Mouse::getPosition(window);
				vManager->UpdateInputs(gManager->GetCurrentPlayer(), coords);
			}

		}		
		window.clear();

		if (gManager->GetGameState() == EGameState::FINISHED)
			window.close();

		vManager->DrawToScreen(window);
		window.display();	


	}	
	
	return 0;
}

