#ifndef _PLAYER_H
#define _PLAYER_H
#include <list>
#include <vector>
using namespace std;

#include "Character.h"
#include "Card.h"
#include "CharacterPool.h"

/*
	Holds all the information required for the player to interact with the game.
*/

class Player
{
public : 
	Player();
	Player(CharacterPool* _cPool);
	Character GetCharacter(int _id);
	Card GetCard(int _id);
	void AddNewCardToHand(Card _card);
	void RemoveCardFromHand(int _cardID);
	EArea GetCharacterPos(int _charID);
	void SetCharacterPos(int _charID, EArea _newPos);
	void RollDie();
	void UseDie(int _dieID);
	vector<int> GetDie();
	bool ActionCheck(EAction _actionID, int _dieID, int _charID, int _cardID, EArea _newPos);
	void CheckForKilledCharacters();


private : 
	void AddCharacter();
	void RemoveCharacter(int _charID);

	CharacterPool* characterPool;
	vector <Character> currentCharacter;
	vector<int> actionDie;
	vector<Card> handDeck;
	
};
#endif
