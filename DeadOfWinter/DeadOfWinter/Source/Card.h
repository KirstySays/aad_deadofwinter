#pragma once

/* 
	Author : Kirsty Fraser 1203064
	Last edited : 16.4.16
	Version : 1.0

	Purpose : Provides the definition for the Card class and handles queries
	of information related to the Card class. 
*/

//System includes
#include <string>
#include <time.h>
#include <stdlib.h>
using namespace std;

//Application includes
#include "Enums.h"

class Card
{
public :
	Card();
	Card(ECardType _type);
	void Init(EArea _area);
	inline ECardType GetType() { return type; }
	inline int GetStatAmount() { return statAmount; }

private : 
	ECardType type;
	int statAmount;
};

