#include "Crisis.h"


void Crisis::GenerateCrisis()
{
	//Random card which isn't food
	srand(time(NULL));

	int randCard = rand() % 5 + 2;
	switch (randCard)
	{
	case 2: 
		type = ECardType::HEALTH;
		break;
	case 3: 
		type = ECardType::GAS;
		break;
	case 4: 
		type = ECardType::WEAPON;
		break;
	case 5: 
		type = ECardType::BOOK;
		break;
	case 6: 
		type = ECardType::JUNK;
		break;
	default: 
		std::cout << "[CRISIS] Exceeded the limit" << std::endl;

	}
	//Random amount
	amount = rand() % 4 + 1;

	//Fail result 
	int randFailResult = rand() % 3 + 1;

	switch (randFailResult)
	{
	case 1: 
		failType = EFailType::SURVIVORS;
		break;
	case 2: 
		failType = EFailType::MORALE;
		break;
	case 3: 
		failType = EFailType::ZOMBIES;
		break;
	default : 
		std::cout << "[CRISIS] Exceeded the limit " << std::endl;
	}

	
}

int Crisis::GetAmount()
{
	return amount;
}

int Crisis::GetFailAmount()
{
	return rand() % 4 + 1;
	
}

EFailType Crisis::GetFailType()
{
	return failType;
}

ECardType Crisis::GetType()
{
	return type;
}
string Crisis::GetDescription()
{
	switch (type)
	{
	case ECardType::BOOK:
		return "Wood is running low and the colony needs something to burn,\n find " + std::to_string(amount) + " books to keep the survivors warm";
		break;
	case ECardType::GAS:
		return "You have found an old generator at the colony however no gas can be found,\n find " + std::to_string(amount) + " gas cannisters to get the generator going";
		break;
	case ECardType::HEALTH:
		return "The hospital is lacking medical supplies for the injured survivors,\n find " + std::to_string(amount) + " health packs to help them out.";
		break;
	case ECardType::JUNK:
		return "Supplies for fixing areas around the colony are running low,\n find " + std::to_string(amount) + " piece of junk so the colony can be repaired";
		break;
	case ECardType::WEAPON : 
		return "There has been some sightings of another group of people near the colony, \nyou should get " + std::to_string(amount) + " weapons to arm your survivors for the fight";
		break;
	}
}