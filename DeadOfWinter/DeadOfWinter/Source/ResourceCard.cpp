#include "ResourceCard.h"

ResourceCard::ResourceCard()
{
	
}
ResourceCard::ResourceCard(ECardType _type, int _value)
{
	cardBG = new SpriteImage(Color::White);
	cardBG->LoadTexture("Resources/cardBG.png");

	typeImage = new SpriteImage(Color::White);

	//Vector2f tempTextPosition = Vector2f(_cardPosition.x - 40, _cardPosition.y + 50);
	cardValueText = new TextField(Color::White);


	//tempTextPosition.y -= 30;
	cardTypeText = new TextField(Color::White);

	string path;
	string tempTypeText;
	switch (_type)
	{
	case ECardType::FOOD : 
		path = "Resources/food.png";
		tempTypeText = "Food";
		break;
	case ECardType::JUNK : 
		path = "Resources/junk.png";
		tempTypeText = "Junk";
		break;
	case ECardType::GAS:
		path = "Resources/gas.png";
		tempTypeText = "Gas";
		break;
	case ECardType::BOOK: 
		path = "Resources/book.png";
		tempTypeText = "Book";
		break;
	case ECardType::HEALTH:	
		path = "Resources/health.png";
		tempTypeText = "Health";
		break;
	case ECardType::WEAPON : 
		path = "Resources/weapon.png";
		tempTypeText = "Weapon";
		break;
	default : 
		std::cout << "Error in card type passed in" << std::endl;
	}

	typeImage->LoadTexture(path);
	string valueStr = std::to_string(_value);
	cardValueText->SetText("Value " + valueStr);

	cardTypeText->SetText(tempTypeText);

}

void ResourceCard::Draw(sf::RenderWindow& window)
{
	window.draw(cardBG->GetSprite());
	window.draw(typeImage->GetSprite());
	window.draw(cardValueText->GetText());
	window.draw(cardTypeText->GetText());
}

void ResourceCard::Move(sf::Vector2f _move)
{
	cardBG->Move(_move);
	Vector2f offset = Vector2f(70, 10);
	typeImage->Move(_move + offset);

	offset = Vector2f(10, 50);
	cardValueText->Move(_move + offset);

	offset = Vector2f(10, 30);
	cardTypeText->Move(_move + offset);
}