#ifndef _GAMEMANAGER_H
#define _GAMEMANAGER_H

#include <list>
#include <vector>
#include "Player.h"
#include "Colony.h"
#include "Area.h"
#include "CharacterPool.h"
#include "Crisis.h"
#include "VisualManager.h"
#include "Enums.h"
using namespace std;

//Responsible for handing the management of the game rounds and set up for the players at the start of the game
//Is also the communication centre for where the visual manager can ask to interact with the base code.

//Forward dec
class VisualManager;

class GameManager
{
public :
	GameManager();
	void CarryOutAction(int _dieID, int charID, EAction _actionID, int _cardID, EArea _newArea);
	void SetUpGame(VisualManager* _vManager);
	void EndOfRoundCheck();
	void DecreaseRoundCounter();
	void UpdateMoraleCounter(int _amount);
	void StartGame();
	void FinishGame(string _reason);
	void ChangeCurrentPlayer();
	inline int GetCurrentPlayer() { return currentPlayer; }
	bool GetGameStarted();
	bool GetGameFinished();

	//THE ABOVE TWO FUNCTIONS COULD BE REPLACED BY THIS
	EGameState GetGameState();

private:

	void CleanUp();
	
	EGameState e_gameState;
	int roundNumber;
	int moraleNumber;
	int currentPlayer;
	bool hasGameStarted;
	bool hasGameFinished;
	Player* playerOne;
	Player* playerTwo;
	Colony* colonyArea;
	vector <Area*> externalAreas;
	CharacterPool characterPool;
	Crisis crisis;
	VisualManager* vManager;

};


#endif

