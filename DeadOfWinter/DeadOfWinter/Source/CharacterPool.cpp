#include "CharacterPool.h"
#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <SFML\System.hpp>
using namespace sf;

CharacterPool::CharacterPool()
{
	//deafult
	std::cout << "CharacterPool constructor called" << endl;
}

Character CharacterPool::GenerateCharacter()
{
	std::cout << "GenerateCharacter called" << std::endl;

	Character nullCharacter;
	srand(time(NULL));

	int randNum = rand() & MAXCHARACTERS + 1;			//TODO set a limit for the amount of characters we're having
	ifstream characterFile;
	//characterFile.open("characters.txt");

	/*string line;
	getline(characterFile,line);
	size_t pos = line.find(randNum);*/
	/*if (pos != string::npos)
	{*/
		//TODO create a parser for the file to the pull the info from
		//Then we have found the line.
		int tempAttack = rand() % 4 + 1 ;
		int tempSearch = rand() % 4 + 1;
		string tempname = "Character";
		
		Character tempCharacter;
		tempCharacter.SetAttackskill(tempAttack);
		tempCharacter.SetPosition(EArea::COLONY);
		tempCharacter.SetSearchSkill(tempSearch);
		tempCharacter.SetName(tempname);
		tempCharacter.ResetWoundCount();
		sleep(sf::milliseconds(1000));
		return tempCharacter;

		

}