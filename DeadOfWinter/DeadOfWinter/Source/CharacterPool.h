#ifndef _CHARACTERPOOL_H
#define _CHARACTERPOOL_H

#include <iostream>
#include <fstream>
using namespace std;

#include "Character.h"

class CharacterPool
{
public : 
	CharacterPool();
	Character GenerateCharacter();

private:
	ofstream nameFile;
	const int MAXCHARACTERS = -1;		//TODO set this 
};

#endif