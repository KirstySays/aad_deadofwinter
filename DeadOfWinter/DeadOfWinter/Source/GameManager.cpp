#include "GameManager.h"


GameManager::GameManager()
{
	//deafult
}
void GameManager::SetUpGame(VisualManager* _vManager)
{
	vManager = _vManager;
	
	playerOne = new Player(&characterPool);
	playerTwo = new Player(&characterPool);

	vector<int> temp;
	Card card;

	colonyArea = new Colony();

	//Generate five starter cards for the players
	for (int cardGenerated = 0; cardGenerated < 5; cardGenerated++)
	{
		Card playerOneTempCard;
		playerOneTempCard.Init(EArea::STARTER);
		playerOne->AddNewCardToHand(playerOneTempCard);
		vManager->AddToPlayerDeck(1, playerOneTempCard.GetType(), playerOneTempCard.GetStatAmount());

		Card playerTwoTempCard;
		playerTwoTempCard.Init(EArea::STARTER);
		playerTwo->AddNewCardToHand(playerTwoTempCard);
		vManager->AddToPlayerDeck(2, playerTwoTempCard.GetType(), playerTwoTempCard.GetStatAmount());
		sleep(sf::milliseconds(2000));
	
	}

	roundNumber = 5;
	moraleNumber = 7;
	currentPlayer = 1;

	crisis.GenerateCrisis();

	Area* policeArea = new Area(EArea::POLICE);
	Area* gasArea = new Area(EArea::GAS);
	Area* groceryArea = new Area(EArea::GROCERY);
	Area* hospitalArea = new Area(EArea::HOSPITAL);
	Area* libraryArea = new Area(EArea::LIBRARY);
	Area* schoolArea = new Area(EArea::SCHOOL);

	externalAreas.push_back(policeArea);
	externalAreas.push_back(gasArea);
	externalAreas.push_back(groceryArea);
	externalAreas.push_back(hospitalArea);
	externalAreas.push_back(libraryArea);
	externalAreas.push_back(schoolArea);
	
	vManager->UpdateFoodCount(colonyArea->GetFoodResources());
	vManager->UpdateCrisisCount(colonyArea->GetCrisisResources());
	vManager->UpdateSurvivors(colonyArea->GetHelplessSurivors());
	vManager->UpdateMorale(moraleNumber);
	vManager->UpdateRound(roundNumber);
	vManager->UpdateZombies(colonyArea->GetZombieCount());
	
	
	vManager->UpdateCharacters(1, 1, playerOne->GetCharacter(0));
	vManager->UpdateCharacters(1, 2, playerOne->GetCharacter(1));
	vManager->UpdateCharacters(2, 1, playerTwo->GetCharacter(0));
	vManager->UpdateCharacters(2, 2, playerTwo->GetCharacter(1));

	
	
}

void GameManager::EndOfRoundCheck()
{
	playerOne->CheckForKilledCharacters();
	playerTwo->CheckForKilledCharacters();
	string message;

	if (!colonyArea->CheckFood())
	{
		UpdateMoraleCounter(-1);
		if (moraleNumber <= 0)
		{
			FinishGame("Game Over, the survivors have lost faith in the colony and have left");
		}
		message = "Failed to feed everyone at colony. Morale has decreased by one. \n";
	}

	vManager->UpdateFoodCount(colonyArea->GetFoodResources());

	DecreaseRoundCounter();
	vManager->UpdateRound(roundNumber);
	if (roundNumber == 0)
	{
		FinishGame("Well done, you survived until rescue arrived");
	}
	
	

	ECardType tempCrisisType = crisis.GetType();
	int tempCrisisAmountt = crisis.GetAmount();
	
	if (!colonyArea->CheckCrisis(tempCrisisType, tempCrisisAmountt))
	{
		//TODO fully complete this out 
		//Get the fail type
		EFailType failure = crisis.GetFailType();
		int failureAmount = crisis.GetFailAmount();
		
		//Apply this to the colony! 
		switch (failure)
		{
		case EFailType::MORALE:
				vManager->UpdateMorale(-1);
				message += "You have failed! Morale has dropped by 1";
				break;
			case EFailType::SURVIVORS:
				colonyArea->AmendHelplessSurvivors(failureAmount);
				vManager->UpdateSurvivors(colonyArea->GetHelplessSurivors());
				message += "You have failed! " + std::to_string(failureAmount) + " survivors have been added.";
				break;
			case EFailType::ZOMBIES:
				colonyArea->AddZombie(failureAmount);
				vManager->UpdateZombies(colonyArea->GetZombieCount());
				message += "You have failed! " + std::to_string(failureAmount) + " zombies have been added.";
				break;
		}

		
	}
	else
	{
		message = "Well done, you succedded! Crisis averted";
		
	}
		
	

	//TODO - figure out how much to add.
	//Add a set amount of zombies
	colonyArea->ResetCrisisResources();
	vManager->UpdateCrisisCount(colonyArea->GetCrisisResources());
	colonyArea->AddZombie(2);
	vManager->UpdateZombies(colonyArea->GetZombieCount());
	vManager->UpdateMorale(moraleNumber);

	if (!colonyArea->CheckZombieCount())
	{
		FinishGame("Game Over. Overun by zombies");
	}
	else
	{
		crisis.GenerateCrisis();

		message += " \n " + crisis.GetDescription();
		vManager->DisplayPopupMessage(message);
		e_gameState = EGameState::POPUP;
	}
	
}

void GameManager::DecreaseRoundCounter()
{
	roundNumber--;
}

void GameManager::UpdateMoraleCounter(int _amount)
{
	moraleNumber += _amount;
}




void GameManager::StartGame()
{
	hasGameStarted = true;
	vManager->DisplayPopupMessage(crisis.GetDescription());
	e_gameState = EGameState::POPUP;

	
}

void GameManager::FinishGame(string _reason)
{
	hasGameFinished = true;
	hasGameStarted = false;
	
	vManager->DisplayPopupMessage(_reason);
	e_gameState = EGameState::POPUP;
	
}

void GameManager::ChangeCurrentPlayer()
{
	if (currentPlayer == 1)
		currentPlayer = 2;
	else
	{
		currentPlayer = 1;
		EndOfRoundCheck();
	}
		
}

void GameManager::CarryOutAction(int _dieID, int _charID, EAction _actionID, int _cardID, EArea _newArea)
{

	if (_actionID == EAction::START)
		StartGame();
	else if (_actionID == EAction::TURN)
		ChangeCurrentPlayer();
	else if (_actionID == EAction::UNPAUSE && hasGameStarted)
		e_gameState = EGameState::PLAYING;
	else if (_actionID == EAction::UNPAUSE && hasGameFinished)
		e_gameState = EGameState::FINISHED;
	else
	{
		Player* curPlayer;
		if (currentPlayer == 1)
			curPlayer = playerOne;
		else
			curPlayer = playerTwo;

		vector<int> die;
		die.empty();

		//EArea curPos = curPlayer->GetCharacterPos(_charID);
		EUpdateID updateID;

		Card tempCard(ECardType::NONE);
		

		if (curPlayer->ActionCheck(_actionID, _dieID, _charID, _cardID, _newArea))
		{
			switch (_actionID)
			{

			case EAction::ATTACK:
				colonyArea->RemoveZombie();
				vManager->UpdateZombies(colonyArea->GetZombieCount());
				break;
			case EAction::SEARCH:
				for (int areaIterator = 0; areaIterator < externalAreas.size(); areaIterator++)
				{
					if (externalAreas.at(areaIterator)->GetName() == _newArea)
					{
						tempCard = externalAreas.at(areaIterator)->DrawNewCard();
						curPlayer->AddNewCardToHand(tempCard);
						vManager->AddToPlayerDeck(currentPlayer, tempCard.GetType(), tempCard.GetStatAmount());
					}
						
				}
				
				break;
			case EAction::MOVE:
				vManager->UpdatePlayersCharacters(currentPlayer, _charID, _newArea);
				break;
		
			case EAction::ROLL:
				vManager->UpdatePlayerDie(currentPlayer, curPlayer->GetDie());
				die = curPlayer->GetDie();
				break;
			case EAction::CARD:
				try
				{
					if (_cardID == 0)
						throw(EErrorType::CARDHIGHLIGHT);
					else
					{
						Card tempCard = curPlayer->GetCard(_cardID);
						colonyArea->RecieveCard(tempCard);
					}
					if (_newArea != EArea::COLONY)
					{
						throw (EErrorType::NOTCOLONY);
					}
					else
					{
						curPlayer->RemoveCardFromHand(_cardID);
						vManager->RemoveFromPlayerDeck(currentPlayer, _cardID);
						vManager->UpdateCrisisCount(colonyArea->GetCrisisResources());
						vManager->UpdateFoodCount(colonyArea->GetFoodResources());
					}
					
				}

				catch (EErrorType error_code)
				{
					if (error_code == EErrorType::CARDHIGHLIGHT)
					{
						cout << "No card highlighted" << endl;
					}
					else if (error_code == EErrorType::NOTCOLONY)
					{
						cout << "Colony has not been selected" << endl;
					}
				}
				
				
			}

		}


		curPlayer = NULL;
		delete curPlayer;
	}
	
}

void GameManager::CleanUp()
{
	playerOne = NULL;
	playerTwo = NULL;
	colonyArea = NULL;
	

	delete(playerOne);
	delete(playerTwo);
	delete(colonyArea);
	
}

bool GameManager::GetGameFinished()
{
	return hasGameFinished;
}

bool GameManager::GetGameStarted()
{
	return hasGameStarted;
}

EGameState GameManager::GetGameState()
{
	return e_gameState;
}