#ifndef _CARDGRAPHIC_H
#define _CARDGRAPHIC_H

#include <SFML/Graphics.hpp>
using namespace sf;

#include "SpriteImage.h"

class CardGraphic
{

protected:
	SpriteImage* cardBG;
	bool isHighlighted;

public : 
	CardGraphic();
	void Draw(sf::RenderWindow&  _window);
	void Align();
	void Update();
	void Highlight();
	void DeHighlight();
	bool IsHighlighted();
	bool CheckBounds(Vector2i mousePos);


};
#endif