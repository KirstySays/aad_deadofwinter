#include "VisualManager.h"

VisualManager::VisualManager()
{
	//TODO add in constructor for AreaGraphic
	AreaGraphic* policeGraphic = new AreaGraphic(EArea::POLICE, this, Vector2f(0, 0));
	AreaGraphic* hospitalGraphic = new AreaGraphic(EArea::HOSPITAL, this, Vector2f(350, 0));
	AreaGraphic* schoolGraphpic = new AreaGraphic(EArea::SCHOOL, this, Vector2f(700, 0));
	AreaGraphic* groceryGraphic = new AreaGraphic(EArea::GROCERY, this, Vector2f(0, 350));
	AreaGraphic* gasGraphic = new AreaGraphic(EArea::GAS, this, Vector2f(350, 350));
	AreaGraphic* libraryGraphic = new AreaGraphic(EArea::LIBRARY, this, Vector2f(700, 350));

	externalAreas.push_back(policeGraphic);
	externalAreas.push_back(hospitalGraphic);
	externalAreas.push_back(schoolGraphpic);
	externalAreas.push_back(groceryGraphic);
	externalAreas.push_back(gasGraphic);
	externalAreas.push_back(libraryGraphic);

	colonyGraphic = new ColonyGraphic(EArea::COLONY);
	startScreen = new StartScreen();

	playerOneMenu = new ActionMenu(this, EPlayer::ONE);
	playerTwoMenu = new ActionMenu(this, EPlayer::TWO);

	popUpMessage = new PopUpMessage(Vector2f(650, 450), this);

	
}

void VisualManager::DrawToScreen(sf::RenderWindow &window)
{
	if (!gManager->GetGameFinished() && !gManager->GetGameStarted())
		startScreen->Draw(window);
	else if (gManager->GetGameStarted() && !gManager->GetGameFinished())
	{
		//Draw something else
		playerOneMenu->Draw(window);
		playerTwoMenu->Draw(window);
		colonyGraphic->Draw(window);

		for each(AreaGraphic* area in externalAreas)
		{
			area->Draw(window);
		}
	}

	if (gManager->GetGameState() == EGameState::POPUP)
		popUpMessage->Draw(window);
}

void VisualManager::Setup(GameManager *_gManager, sf::RenderWindow &_window)
{
	gManager = _gManager;

	startScreen->SetUp(this);
	startScreen->Draw(_window);
}
void VisualManager::UpdateFoodCount(int _amount)
{
	colonyGraphic->UpdateFoodText(_amount);
}
void VisualManager::UpdateZombies(int _amount)
{
	colonyGraphic->UpdateZombies(_amount);
}

void VisualManager::UpdateCrisisCount(int _amount)
{
	colonyGraphic->UpdateContribution(_amount);
}

void VisualManager::UpdateSurvivors(int _count)
{
	colonyGraphic->UpdateSurvivors(_count);
}

void VisualManager::UpdateMorale(int _morale)
{
	colonyGraphic->UpdateMoraleNumbers(_morale);
}

void VisualManager::UpdateRound(int _round)
{
	colonyGraphic->UpdateRoundNumber(_round);
}

void VisualManager::UpdateCharacters(int _playerID, int _charID, Character _character)
{
	if (_playerID == 1)
		playerOneMenu->UpdateCharacter(_charID, _character);
	else if (_playerID == 2)
		playerTwoMenu->UpdateCharacter(_charID, _character);
}

void VisualManager::UpdateInputs(int _playerID, sf::Vector2i _mousePos)
{

	if (gManager->GetGameState() == EGameState::POPUP)
	{
		popUpMessage->CheckForInput(_mousePos);
	}
	if (_playerID == 1)
		playerOneMenu->CheckInputs(_mousePos);
	else if (_playerID == 2)
		playerTwoMenu->CheckInputs(_mousePos);

	colonyGraphic->CheckInput(_mousePos);
	for each (AreaGraphic* area in externalAreas)
	{
		area->CheckInput(_mousePos);
	}
	startScreen->CheckForInput(_mousePos);
}

void VisualManager::AddToPlayerDeck(int _playerID, ECardType _type, int _value)
{
	//TODO review the parameters with the actionmenu->addcard
	if (_playerID == 1)
		playerOneMenu->AddCard(_type, _value);
	else if (_playerID == 2)
		playerTwoMenu->AddCard(_type, _value);
	else
		std::cout << "[VISUAL MANAGER]Error adding card to player, no player ID given " << endl;

}

void VisualManager::UpdatePlayerDie(int _playerID, vector<int> _actionDie)
{
	if (_playerID == 1)
		playerOneMenu->UpdateDie(_actionDie);
	else if (_playerID == 2)
		playerTwoMenu->UpdateDie(_actionDie);


}

void VisualManager::UpdatePlayersCharacters(int _playerID, int _characterID, EArea _newArea)
{
	ECharColour color;
	color = ECharColour::NONE;

	//Get the colour
	if (_playerID == 1)
		color = playerOneMenu->GetCharacterColor(_characterID);
	else if (_playerID == 2)
		color = playerTwoMenu->GetCharacterColor(_characterID);

	//Update the previous area 
	for (int areaIterator = 0; areaIterator < 6; areaIterator++)
	{
		externalAreas[areaIterator]->RemoveCharacter(color);
		
	}

	colonyGraphic->RemoveCharacter(color);


	//Update the new area with the color needed
	if (colonyGraphic->GetArea() == _newArea)
		colonyGraphic->AddCharacter(color);

	for (int areaIterator = 0; areaIterator < 6; areaIterator++)
	{
	
		if (_newArea == externalAreas[areaIterator]->GetAreaType())
			externalAreas[areaIterator]->AddCharacter(color);
		
	}
	
	
	
}
void VisualManager::RemoveFromPlayerDeck(int _playerID, int _cardID)
{
	if (_playerID == 1)
		playerOneMenu->RemoveCard(_cardID);
	else if (_playerID == 2)
		playerTwoMenu->RemoveCard(_cardID);
}

void VisualManager::UpdateAreas(int _charID, string _oldPos, string _newPos)
{
	
}

void VisualManager::SendToGameManager(int _dieID, int _charID, EAction _actionID, int _cardID, EArea _newArea)
{
	if (EAction::MOVE == _actionID || EAction::ATTACK == _actionID || EAction::SEARCH == _actionID)
	{
		try
		{
			//Collect the area that is highlighted
			for each (AreaGraphic* area in externalAreas)
			{
				if (area->IsHighlighted())
					_newArea = area->GetAreaType();
			}
			if (colonyGraphic->IsHighlighted())
				_newArea = colonyGraphic->GetArea();
			else if (_newArea == EArea::NONE)
			{
				throw (EErrorType::AREAHIGHLIGHT);

			}
		}
		
		catch (EErrorType error_code)
		{
			if (error_code == EErrorType::AREAHIGHLIGHT)
			{
				cout << "No Area Selected" << endl;
			}
		}

		
	}
	else if (EAction::CARD == _actionID)
	{
		try
		{
			//Collect the area
			if (colonyGraphic->IsHighlighted())
				_newArea = colonyGraphic->GetArea();
			else if (_newArea == EArea::NONE)
			{
				throw (EErrorType::AREAHIGHLIGHT);

			}

			else if (_cardID == 0)
			{
				throw (EErrorType::CARDHIGHLIGHT);
			}

			
		}
		catch (EErrorType error_code)
		{
			if (error_code == EErrorType::AREAHIGHLIGHT)
			{ 
				cout << "[CARD PLACEMENT] Area selected isn't Colony / No Area selected" << endl;
			}
			else if (error_code == EErrorType::CARDHIGHLIGHT)
			{
				cout << "[CARD PLACEMENT] No Card highlighted" << endl;
			}
		}
		
		
		
	}
	
	gManager->CarryOutAction(_dieID, _charID, _actionID, _cardID, _newArea);

}

void VisualManager::DisplayPopupMessage(string _message)
{
	popUpMessage->UpdateMessageText(_message);
}

