#ifndef _VISUALMANAGER_H
#define _VISUALMANAGER_H

#include <vector>
#include <string>
using namespace std;

#include "GameManager.h"
#include "ActionMenu.h"
#include "AreaGraphic.h"
#include "ColonyGraphic.h"
#include "PopUpMessage.h"
#include "StartScreen.h"
#include "Enums.h"

class GameManager;
class ActionMenu;
class AreaGraphic;
class ColonyGraphic;
class PopUpMessage;
class StartScreen;

class VisualManager{
public : 
	VisualManager();
	void DrawToScreen(sf::RenderWindow &window);
	void Setup(GameManager *_gManager, sf::RenderWindow &_window);
	void UpdateZombies(int _amount);
	void UpdateFoodCount(int);
	void UpdateCrisisCount(int);
	void UpdateSurvivors(int);
	void UpdateMorale(int);
	void UpdateRound(int);
	void UpdateCharacters(int _playerID, int _charID, Character _character);
	void UpdateInputs(int currentPlayer, sf::Vector2i _mousPos);
	void AddToPlayerDeck(int _playerID, ECardType _cType, int _value);
	void UpdatePlayersCharacters(int _playerID, int _characterID, EArea _newArea);
	void UpdatePlayerDie(int _playerID, vector<int> _actionDie);
	void RemoveFromPlayerDeck(int _playerID, int _cardID);
	void UpdateAreas(int _charID, string _oldPos, string _newPos);
	void SendToGameManager(int _dieID, int _charOD, EAction _actionID, int _cardID, EArea _newArea);
	void DisplayPopupMessage(string _message);


private: 
	GameManager *gManager;
	ActionMenu* playerOneMenu;
	ActionMenu* playerTwoMenu;
	vector<AreaGraphic*> externalAreas;
	ColonyGraphic* colonyGraphic;
	PopUpMessage* popUpMessage;
	StartScreen* startScreen;
};
#endif