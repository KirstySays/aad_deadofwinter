#include "Area.h"

/*
	Basic constructor for the class
	@param N.A
	@return N.A
*/
Area::Area()
{

}

/*
	Overloaded constructor which is able to define the name for the area
	@param _name : Gives us the area that we're defining for through this object
	@return N.A
*/
Area::Area(EArea _name)
{
	name = _name;
}

/* 
	 Asks for a new card to be generated based on the probablities from this area type
	 @param N.A
	 @return tempCard : the newly generated Card object based on this area.
*/
Card Area::DrawNewCard()
{
	Card tempCard;
	tempCard.Init(name);
	
	return tempCard;

}