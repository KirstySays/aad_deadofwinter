#ifndef CRISIS_H
#define CRISIS_H

#include <time.h>
#include <iostream>
#include <string>

#include "Enums.h"

using namespace std;
class Crisis 
{
public :
	ECardType GetType();
	EFailType GetFailType();
	int GetAmount();
	int GetFailAmount();
	void GenerateCrisis();
	string GetDescription();

private : 
	ECardType type;
	EFailType failType;
	int amount;
};
#endif
