#include "ColonyGraphic.h"

ColonyGraphic::ColonyGraphic(EArea _area)
{
	slotOneImage = new SpriteImage(sf::Color::White, sf::Vector2f(1250, 250));
	slotOneImage->LoadTexture("Resources/White.png");
	

	slotTwoImage = new SpriteImage(sf::Color::White, sf::Vector2f(1250, 310));
	slotTwoImage->LoadTexture("Resources/White.png");

	slotThreeImage = new SpriteImage(sf::Color::White, sf::Vector2f(1250, 370));
	slotThreeImage->LoadTexture("Resources/White.png");

	slotFourImage = new SpriteImage(sf::Color::White, sf::Vector2f(1250, 430));
	slotFourImage->LoadTexture("Resources/White.png");
	
	foodText = new TextField(sf::Color::White, sf::Vector2f(1350, 250));
	foodText->LoadFont("Resources/Vec.ttf");
	foodText->SetText("Food : ");

	crisisContribution = new TextField(sf::Color::White, sf::Vector2f(1350, 300));
	crisisContribution->LoadFont("Resources/Vec.ttf");
	crisisContribution->SetText("Crisis : ");

	
	survivorCount = new TextField(sf::Color::White, sf::Vector2f(1350, 350));
	survivorCount->LoadFont("Resources/Vec.ttf");
	survivorCount->SetText("Survivors : ");

	zombieCount = new TextField(sf::Color::White, sf::Vector2f(1350, 400));
	zombieCount->LoadFont("Resources/Vec.ttf");
	zombieCount->SetText("Zombies : ");

	barrierCount = new TextField(sf::Color::White, sf::Vector2f(1350, 450));
	barrierCount->LoadFont("Resources/Vec.ttf");
	barrierCount->SetText("Barriers : ");


	roundText = new TextField(sf::Color::White, sf::Vector2f(1350, 500));
	roundText->LoadFont("Resources/Vec.ttf");
	roundText ->SetText("Round : ");

	moraleText = new TextField(sf::Color::White, sf::Vector2f(1350, 550));
	moraleText->LoadFont("Resources/Vec.ttf");
	moraleText->SetText("Morale : ");
	
	backgroundImage = new SpriteImage(sf::Color::White, sf::Vector2f(1200, 200));
	backgroundImage->LoadTexture("Resources/glassPanel.png");
	backgroundImage->SetScale(Vector2f(4, 4));

	areaImage = new SpriteImage(sf::Color::White, Vector2f(1500, 200));
	areaImage->LoadTexture("Resources/castle_small.png");

	areaText = new TextField(sf::Color::Black, Vector2f(1300, 200));
	areaText->LoadFont("Resources/Vec.ttf");
	areaText->SetText("Colony (Base)");

	area = _area;
	
}

void ColonyGraphic::UpdateZombies(int _count)
{
	string valueStr = std::to_string(_count);
	zombieCount->SetText("Zombies : " + valueStr);
}

void ColonyGraphic::AddCharacter(ECharColour _charID)
{
	Color newColor;
	switch (_charID)
	{
	case ECharColour::BLUE:
		newColor = Color::Blue;
		break;
	case ECharColour::GREEN:
		newColor = Color::Green;
		break;
	case ECharColour::RED:
		newColor = Color::Red;
		break;
	case ECharColour::YELLOW:
		newColor = Color::Yellow;
		break;
	}

	if (charactersPresent < MAXCHARALLOCATION)
	{
		if (charactersPresent == 0)
		{
			slotOneImage->SetColour(newColor);
			//charactersPresent++;
		}
		else if (charactersPresent == 1)
		{
			slotTwoImage->SetColour(newColor);
			//charactersPresent++;
		}
		else if (charactersPresent == 2)
		{
			slotThreeImage->SetColour(newColor);
			//charactersPresent++;
		}
		else
			slotFourImage->SetColour(newColor);
		charactersPresent++;


	}
}

void ColonyGraphic::RemoveCharacter(ECharColour _charID)
{
	Color colorToCheck;
	colorToCheck == Color::White;
	//Check if this area has that color being used. 
	switch (_charID)
	{
	case ECharColour::BLUE:
		colorToCheck = Color::Blue;
		break;
	case ECharColour::GREEN:
		colorToCheck = Color::Green;
		break;
	case ECharColour::RED:
		colorToCheck = Color::Red;
		break;
	case ECharColour::YELLOW:
		colorToCheck = Color::Yellow;
		break;

	}

	if (slotOneImage->GetSprite().getColor() == colorToCheck)
	{

		slotOneImage->SetColour(Color::White);
		charactersPresent--;
	}
	else if (slotTwoImage->GetSprite().getColor() == colorToCheck)
	{
		slotTwoImage->SetColour(Color::White);
		charactersPresent--;
	}
	else if (slotThreeImage->GetSprite().getColor() == colorToCheck)
	{
		slotThreeImage->SetColour(Color::White);
		charactersPresent--;
	}
	else if (slotFourImage->GetSprite().getColor() == colorToCheck)
	{
		slotFourImage->SetColour(Color::White);
		charactersPresent--;
	}

}

void ColonyGraphic::Draw(sf::RenderWindow& window)
{
	window.draw(backgroundImage->GetSprite());
	window.draw(areaImage->GetSprite());
	window.draw(slotOneImage->GetSprite());	
	window.draw(slotTwoImage->GetSprite());
	window.draw(slotThreeImage->GetSprite());
	window.draw(slotFourImage->GetSprite());

	window.draw(foodText->GetText());
	window.draw(crisisContribution->GetText());
	window.draw(survivorCount->GetText());
	window.draw(zombieCount->GetText());
	window.draw(barrierCount->GetText());
	window.draw(roundText->GetText());
	window.draw(moraleText->GetText());
	window.draw(areaText->GetText());

	
}
void ColonyGraphic::UpdateSurvivors(int _count)
{
	string valueStr = std::to_string(_count);
	survivorCount->SetText("Survivors : " + valueStr);
}

void ColonyGraphic::UpdateFoodText(int _count)
{
	string valueStr = std::to_string(_count);
	foodText->SetText("Food : " + valueStr);
}

void ColonyGraphic::UpdateContribution(int _count)
{
	string valueStr = std::to_string(_count);
	crisisContribution->SetText("Crisis : " + valueStr);
}

void ColonyGraphic::UpdateRoundNumber(int _round)
{
	string valueStr = std::to_string(_round);
	roundText->SetText("Round : " + valueStr);
}

void ColonyGraphic::UpdateMoraleNumbers(int _morale)
{
	string valueStr = std::to_string(_morale);
	moraleText->SetText("Morale : " + valueStr);
}

void ColonyGraphic::CheckInput(sf::Vector2i mousePos)
{
	if (backgroundImage->CheckBounds(mousePos))
	{
		cout << "Clicking on colony " << endl;
		Highlight();
	}

	else
		DeHighlight();
}

void ColonyGraphic::Highlight()
{
	isHighlighted = true;
	backgroundImage->SetColour(Color::Cyan);
}

void ColonyGraphic::DeHighlight()
{
	isHighlighted = false;
	backgroundImage->SetColour(Color::White);
}