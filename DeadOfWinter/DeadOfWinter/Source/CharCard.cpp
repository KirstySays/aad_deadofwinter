#include "CharCard.h"

CharCard::CharCard(ECharColour _color, Vector2f _startPosition)
{
	charColor = _color;	

	cardBG = new SpriteImage(Color::White, _startPosition);
	cardBG->LoadTexture("Resources/cardBG.png");

	charSprite = new SpriteImage(Color::White, Vector2f(_startPosition.x + 50 ,_startPosition.y+ 100));
	string filePath;
	switch (charColor)
	{
	case ECharColour::BLUE:
		filePath = "Resources/Blue.png";
		break;
	case ECharColour::GREEN:
		filePath = "Resources/Green.png";
		break;
	case ECharColour::RED:
		filePath = "Resources/Red.png";
		break;
	case ECharColour::YELLOW:
		filePath = "Resources/Yellow.png";
		break;
	}
	charSprite->LoadTexture(filePath);

	nameText = new TextField(Color::White, Vector2f(_startPosition.x + 15, _startPosition.y + 10));
	attackText = new TextField(Color::White, Vector2f(_startPosition.x + 15, _startPosition.y + 30));
	searchText = new TextField(Color::White, Vector2f(_startPosition.x + 15, _startPosition.y + 50));

	

}

void CharCard::Draw(sf::RenderWindow& window)
{

	window.draw(cardBG->GetSprite());
	window.draw(charSprite->GetSprite());
	window.draw(nameText->GetText());
	window.draw(attackText->GetText());
	window.draw(searchText->GetText());

}

void CharCard::Align()
{
	
}

void CharCard::AlignComponents()
{
	
}

void CharCard::SetCharacterSpriteImage()
{
	int randImage = rand() % 6;		//TODO set the amount of images that we're going to have

	string charFilePath = "char" + randImage;
	charFilePath.append(".png");

}

void CharCard::SetAttackText(int _value)
{
	string valueStr = std::to_string(_value);
	attackText->SetText("Attack : " + valueStr);
}

void CharCard::SetSearchText(int _value)
{
	string valueStr = std::to_string(_value);
	searchText->SetText("Search : " + valueStr);
}


