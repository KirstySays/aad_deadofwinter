#ifndef POPUPMESSAGE_H
#define POPUPMESSAGE_H

#include <SFML\Graphics.hpp>
#include <string>
#include "TextField.h"
#include "SpriteImage.h"
#include "VisualManager.h"
using namespace std;
using namespace sf;

class PopUpMessage
{
public : 
	PopUpMessage();
	PopUpMessage(Vector2f _startPos, VisualManager* _vManager);
	void UpdateMessageText(string _message);
	void Draw(sf::RenderWindow& window);
	void Clear();
	void CheckForInput(Vector2i mousePos);
private: 
	TextField* messageText;
	SpriteImage* backgroundImage;
	SpriteImage* buttonSprite;
	VisualManager* vManager;

	const Vector2f MESSAGETEXTOFFSET = Vector2f(50, 30);
	const Vector2f BUTTONOFFSET = Vector2f(300, 110);
};
#endif